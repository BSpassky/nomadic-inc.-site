class ContactMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.contact.subject
  #
  def contact(message)
    @message = message
    @url = 'http://www.google.com'
    mail(to: @message.email, subject: 'Recieved Inquiry')


  end
  
  def mail_alert(message)
    @message = message
    @url = 'http://www.google.com'
    mail(to: "nsellajr@gmail.com", subject: "Success! You did it.")
    
  end
    
end
