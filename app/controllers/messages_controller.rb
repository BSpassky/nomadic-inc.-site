class MessagesController < ApplicationController
    
    def show
        if flash[:redirect] == 'form_submitted'
          @message = Message.find(params[:id])
        else
          redirect_to pages_home_path
        end
    end
    
    def new
        @message = Message.new
    end
    
    def create
        @message = Message.new(message_params)
        if @message.save
            ContactMailer.contact(@message).deliver
            ContactMailer.mail_alert(@message).deliver
            flash[:redirect] = 'form_submitted'
            redirect_to @message
        else
        render 'new'
        
        end
        
    end
    
    private
    
      def message_params
          params.require(:message).permit(:name,:email,:inquiry)
      end
end
