class ContactsController < ApplicationController
    
    def show
        @contact = Message.find(params[:id])
    end
    
    def new
       @contact = Message.new
    end
    
    def create
      @contact = Message.new(contact_params)
      if @contact.save
          redirect_to :action => 'show'
      else
      render :action => 'new'
      end
          
    end
    
    
    private
    
      def contact_params
          params.require(:user).permit(:name,:email,:inquiry)
      end
end
